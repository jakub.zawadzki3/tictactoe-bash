# tictactoe-bash

Simple tic tac toe game implemented in bash.

## How to play

Just execute tictactoe.sh script.

## Features

* Save the game
* Load last save
* Play with AI
