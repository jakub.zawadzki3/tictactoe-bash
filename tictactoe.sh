#!/bin/bash
NUMBER_REGEX='^[0-9]+$'
BOARD=("1" "2" "3" "4" "5" "6" "7" "8" "9")
WIN="N"
player=0
ai=0
ai_player=0
move=0
loaded_game=0

function print_board() {
    if [ $ai -eq 1 ]; then echo "Grasz z AI."; fi
    j=1
    for i in "${BOARD[@]}"
    do
      echo -n "$i |"
      if [ $((j % 3)) -eq 0 ]
      then
        echo ""
        echo "----------"
      fi
      ((j=j+1))
    done
}

function save_game() {
  echo "${BOARD[@]}"  > game.save
  echo $player >> game.save
  if [ $ai -eq 1 ]
  then
    echo $ai_player >> game.save
  else
    echo '-1' >> game.save
  fi
}

function load_game() {
  IFS=' ' read -r -a BOARD < game.save
  player=$(tail -n 2 game.save | head -n 1)
  ai_player=$(tail -n 1 game.save)
  if [ $ai_player -eq -1 ]
  then
    ai=0
  else
    ai=1
  fi
  loaded_game=1
}

function print_move_prompt() {
  if [ $player -eq 0 ]; then mark="O"; else mark="X"; fi
  echo "Ruch gracza $mark. Podaj numer pola, lub 0 aby zapisać grę i wyjść."
  read move
}

function print_load_game_prompt() {
echo "Wcisnij 1, aby załadować ostatnią zapisaną grę."
read check_load
if [ $check_load -eq 1 ]
then
 load_game
fi
}

function print_enable_ai_prompt() {
  echo "Wcisnij 1, aby uruchomić AI."
  read check_ai
  if [ $check_ai -eq 1 ]
  then
   ai=1
   ai_player=$(($RANDOM % 2))
  fi
}

function ai_move() {
  move=$((1 + $RANDOM % 8))
  while [ "${BOARD[$move - 1]}" = "X" ] || [ "${BOARD[$move - 1]}" = "O" ]
  do
    ((move=1 + $move % 8 ))
  done
}

function check_save() {
  if [ $move -eq 0 ]
  then
    save_game
    exit 0
  fi
}

function player_move() {
  clear
  print_board
  print_move_prompt
  check_save
  while  [[ ! $move =~ $NUMBER_REGEX ]] || [ $move -lt 1 ] || [ $move -gt 9 ] || [ "${BOARD[$move - 1]}" = "X" ] || [ "${BOARD[$move - 1]}" = "O" ]
  do
    clear
    echo "Błędny ruch, spróbuj jeszcze raz."
    print_board
    print_move_prompt
    check_save
  done
}

function update_board() {
  if [ $player -eq 0 ]
  then
    BOARD[move-1]="O"
  else
    BOARD[move-1]="X"
  fi
}

function check_win() {
  for i in {0..2}
  do
    if [ "${BOARD[i*3]}" = "${BOARD[i * 3 + 1]}" ] && [ "${BOARD[i * 3 + 1]}" = "${BOARD[i * 3 + 2]}" ]
    then
      WIN="${BOARD[i*3]}"
    fi
    if [ "${BOARD[i]}" = "${BOARD[i + 3]}" ] && [ "${BOARD[i + 3]}" = "${BOARD[i + 6]}" ]
    then
      WIN="${BOARD[i]}"
    fi
  done
  if [ "${BOARD[0]}" = "${BOARD[4]}" ] && [ "${BOARD[4]}" = "${BOARD[8]}" ]
  then
    WIN="${BOARD[0]}"
  fi
  if [ "${BOARD[2]}" = "${BOARD[4]}" ] && [ "${BOARD[4]}" = "${BOARD[6]}" ]
  then
    WIN="${BOARD[2]}"
  fi
}



if [ -f game.save ]
then
  print_load_game_prompt
  clear
fi
if [ $loaded_game -eq 0 ]
then
  print_enable_ai_prompt
  clear
fi
while [ $WIN = "N" ]
do
  if [ $ai -eq 1 ] && [ "$player" -eq "$ai_player" ]
  then
    ai_move
  else
    player_move
  fi
  update_board
  check_win
  ((player=(player + 1) % 2))
done
clear
print_board
echo "Wygrał $WIN."
